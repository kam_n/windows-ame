#!/usr/bin/env bash

backup_script=backup.sh
restore_script=restore.sh
remove_script=remove.sh
find_list=find_list.txt

clear
echo "                 ╔═══════════════╗"
echo "                 ║ !!!WARNING!!! ║"
echo "╔════════════════╩═══════════════╩══════════════════╗"
echo "║ This script comes without any warranty.           ║"
echo "║ If your computer no longer boots, explodes, or    ║"
echo "║ divides by zero, you are the only one responsible ║"
echo "╚═══════════════════════════════════════════════════╝"
echo ""
read -p "To continue press [ENTER], or Ctrl-C to exit"

title_bar() {
	clear
	echo "╔═════════════════════════════════════════════════════╗"
	echo "║ AMEliorate Windows 10 2004               2020.10.31 ║"
	echo "╚═════════════════════════════════════════════════════╝"
	echo ""
}

ame_backup_check() {
	echo "Checking for existing AME Backup"
	local FILE=./AME_Backup/
	if [ -d $FILE ]; then
		now=$(date +"%Y.%m.%d.%H.%M")
		tar cfz AME_Backup_$now.tar.gz AME_Backup/
		rm -rf AME_Backup/
	else
   		echo "$FILE' not found, continuing"
	fi
}

title_bar
ame_backup_check


set_ame_regex() {
    ame_regex=$(echo "${1}" | \
    awk 'BEGIN {
          RS = " ";term = ""
    }
    {
          term = (term "\\|" $0)
    }
    END {
          res = substr(term, 3, length(term) - 3)
          print ".*\\(" res "\\).*"
    }')
}

# start AME process
title_bar
echo "Starting AME process, searching for files..."
terms="autologger clipsvc clipup DeliveryOptimization DeviceCensus.exe diagtrack dmclient dosvc EnhancedStorage homegroup hotspot invagent microsoftedge.exe msra sihclient slui startupscan storsvc usoclient usocore windowsmaps windowsupdate wsqmcons wua wus"
set_ame_regex "${terms}"
find . -regextype sed -iregex "${ame_regex}" -type f | awk '!/FileMaps|WinSxS|MSRAW|msrating/' > ${find_list}

if [ ! -s ${find_list} ]
then
	echo "ERROR! no files found, exiting..."
	exit 1	
fi


ind_places_arr=(
    "Program Files/Internet Explorer"
    "Program Files/Windows Defender"
    "Program Files/Windows Mail"
    "Program Files/Windows Media Player"
    "Program Files (x86)/Internet Explorer"
    "Program Files (x86)/Windows Defender"
    "Program Files (x86)/Windows Mail"
    "Program Files (x86)/Windows Media Player"
    "Windows/System32/wua*"
    "Windows/System32/wups*"
    "Windows/SystemApps/*CloudExperienceHost*"
    "Windows/SystemApps/*ContentDeliveryManager*"
    "Windows/SystemApps/Microsoft.MicrosoftEdge*"
    "Windows/SystemApps/Microsoft.Windows.Cortana*"
    "Windows/SystemApps/Microsoft.XboxGameCallableUI*"
    "Windows/System32/smartscreen.exe"
    "Windows/System32/smartscreenps.dll"
    "Windows/diagnostics/system/Apps"
    "Windows/diagnostics/system/WindowsUpdate"
    "Windows/SystemApps/Microsoft.XboxIdentityProvider*"
    "Windows/System32/SecurityHealthAgent.dll"
    "Windows/System32/SecurityHealthService.exe"
    "Windows/System32/SecurityHealthSystray.exe")

get_escaped() {
    val="${1// /\\ }" # escape spaces
    val="${val//(/\\(}" # escape parentheses
    val="${val//)/\\)}"
    echo "${val}"
}

# creates backup script
cat<<-EOF > ${backup_script}
	#!/usr/bin/env bash
	mkdir -p AME_Backup
EOF
# adds individual directories to the backup script
for elem in "${ind_places_arr[@]}"; do
    val=$(get_escaped "${elem}")
    echo "cp -fa --preserve=all --parents ${val} AME_Backup" >> ${backup_script}
done
# adds all the rest
awk -v quote='"' '{print "cp -fa --preserve=all --parents " quote $0 quote " " "AME_Backup"}' ${find_list}  >> ${backup_script}
chmod +x ${backup_script}

# creates restore script
cat<<-EOF > ${restore_script}
	#!/usr/bin/env bash
	echo "This script will restore all the necessary files for Windows Updates to be installed manually"
	read -p "To continue press [ENTER], or Ctrl-C to exit"
	(cd AME_Backup
EOF
for elem in "${ind_places_arr[@]}"; do
    val=$(get_escaped "${elem}")
    echo "cp -fa --preserve=all --parents ${val} .." >> ${restore_script}
done
echo ")" >> ${restore_script}
awk -v quote='"' '{print "cp -fa --preserve=all " quote "AME_Backup/" $0 quote " " quote $0 quote}' ${find_list} >> ${restore_script}
chmod +x ${restore_script}

# creates removal script
cat<<-EOF > ${remove_script}
	#!/usr/bin/env bash
EOF
awk -v quote='"' '{print "rm -rf " quote $0 quote}' ${find_list} > ${remove_script}
for elem in "${ind_places_arr[@]}"; do
    val=$(get_escaped "${elem}")
    echo "rm -rf ${val}" >> ${remove_script}
done
chmod +x ${remove_script}


title_bar
echo "Backing up files"
./${backup_script}
echo "Done."
echo "Removing files"
./${remove_script}
echo "Done."
sync
title_bar
rm ${find_list}
echo "You may now reboot into Windows"
